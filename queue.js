let collection = [];

// Write the queue functions below.

function enqueue(i){

collection[collection.length] = i

return collection
}

function dequeue() {
  
  collection.shift()
  	return collection;
	}

function front() {
	return collection[0];
}

function size(){
	return collection.length;
}

function print() {
  return [...collection];
}

function isEmpty(){
	return collection.length === 0;
}

module.exports = {
enqueue,
dequeue,
front,
size,
print,
isEmpty
};